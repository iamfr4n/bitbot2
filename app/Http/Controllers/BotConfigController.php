<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Data;

class BotConfigController extends Controller
{
    public function show()
    {
        $currencies = [
         'EUR' => 'EUR',
         'GBP' => 'GBP',
         'USD' => 'USD',         
         'ZAR' => 'ZAR'
        ];
        $data=Data::all()->where('user_id', auth()->user()->id);
        if ( count($data) == 0 ) {
        	$data->bot_currency = "";
        	$data->telegram_username = "";
        	$data->contact_tel = "";
        } else {
        	$data = $data->first();
        }
        return view('botconfig/view_botconfig')->withCurrencies($currencies)->withData($data);
    }
    public function store(Request $request)
    {
        // dd(request()->all());
        // $post = new Post;
        // $post->user_id = auth()->user()->id;
        // $post->bot_currency = request('w_bot_currency');
        // $post->telegram_username = request('w_telegram_username');
        // $post->contact_tel = request('w_user_contact_tel');
        // $post->save();
        Post::updateOrCreate(['user_id'=>auth()->user()->id],['user_id'=>auth()->user()->id,'bot_currency'=>request('w_bot_currency'),'telegram_username'=>request('w_telegram_username'),'contact_tel'=>request('w_user_contact_tel')]);
        return redirect('/home');    
    }   
}
