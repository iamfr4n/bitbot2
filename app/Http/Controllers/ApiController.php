<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Telegram\Bot\Api;
use App\Data;

class ApiController extends Controller
{
	public function setWebHook() {
		$telegram = new Api(env('TELEGRAM-BOT-TOKEN'));
		$response = $telegram->setWebhook(['url' => 'https://anchorsoft.co.za/bitbot2/331819316:AAHu685JGc6zRiLwHIHkzVVyKdgjUWBUarU/webhook']);
		return $response;
	}
	public function webhook(Request $request){
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
		$chatid = $request['message']['chat']['id'];
		$text = $request['message']['text'];
		$telegram_username = (!empty($request['message']['from']['username'])) ? $request['message']['from']['username']  : "";

		if (strstr($text,"getBTCEquivalent")) {
			$this->getBTCEquivalent($telegram, $chatid, $text, $telegram_username);
		} else {
			switch($text) {
				case '/start':
					// $this->showMenu($telegram, $chatid);
					$info = "Welcome to BitBot ".$request['message']['from']['first_name']." \xF0\x9F\x98\x8E"."\n";
					$info.= "BitBot is a Telegram Messenger bot which returns BitCoin Rates and (registered) user information.\n";
					$info.= "BitBot can be configured by registering on this website: https://anchorsoft.co.za/bitbot2\n\n";
					$info.= "Please choose from the following options:\n\n";
		            $info.= "To display the main menu:\n";
					$info.= "/help or /menu\n";
		            $this->showMenu($telegram, $chatid, $info);
					break;
			    case '/help':
					$info = "To display the main menu:\n";
					$info.= "/help or /menu\n";
			        $this->showMenu($telegram, $chatid, $info);
			        break;				
				case '/menu':
				    $this->showMenu($telegram, $chatid);
				    break;
				case '/getUserID';
					$info = "Please visit https://anchorsoft.co.za/bitbot2 to link your Telegram username to BitBot";
					if (!empty($telegram_username)) {
						$result=Data::where('telegram_username', $telegram_username)->first();
						if ( count($result) != 0 ) {
							$info = "PayBee UserID: ".$result->id;
						}
					}
				    $this->getUserID($telegram, $chatid, $info);
				    break;				
				default:
		            $info = "I do not understand what you just said."."\xF0\x9F\x98\x95"."\nPlease choose an option:\n";
		            $info.= "To display the main menu:\n";
					$info.= "/help or /menu\n\n";
					$this->showMenu($telegram, $chatid, $info);
			}
		}
	}
	public function showMenu($telegram, $chatid, $info = null){
	    $message = '';
	    if($info !== null){
	        $message .= $info.chr(10);
	    }
	    $message.= "Return current BTC rate:\n";
	    $message.= "/getBTCEquivalent [amt] [curr]\n";
	    $message.= "[amt] = amount your wish to query (default 1)\n";
	    $message.= "[curr] = currency to convert to (default USD)\n";
	    $message.= "available options: USD/EUR/GBP\n";
	    $message.= "\n";
	    $message.= "Return your [userid]:\n";
	    $message.= "/getUserID\n";

	    $response = $telegram->sendMessage([
	        'chat_id' => $chatid, 
	        'text' => $message
	    ]);
	}

	public function getBTCEquivalent($telegram, $chatid, $info = null, $telegram_username){
        $w_amount = 1;
        $w_curr = "USD";
        if (!empty($telegram_username)) {
        	$result=Data::where('telegram_username', $telegram_username)->first();
			if ( count($result) != 0 ) {
                $w_curr = strtoupper($result->bot_currency);
			}
        }
        if($info !== null) {
                $my_arr = explode(" ",$info);
                if ( !empty($my_arr[1]) && is_numeric($my_arr[1]) ) {
                        $w_amount = $my_arr[1];
                }
                if ( !empty($my_arr[2]) ) {
                        $w_curr = strtoupper($my_arr[2]);
                }
        }
		$url = "https://api.coindesk.com/v1/bpi/currentprice/".$w_curr.".json";
		$html = file_get_contents($url);
		if (sizeof($html) == 1) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$data = curl_exec($ch);
			curl_close($ch);
			$html = $data;
		}
		$json_data = json_decode($html,true);
		$w_rate_float = $json_data["bpi"][$w_curr]["rate_float"];
		$w_rate = $json_data["bpi"][$w_curr]["rate"];
		$message = $w_amount." ".$w_curr." is ";
		$message.= number_format( ($w_amount / $w_rate_float),2 );
		$message.= " BTC (".number_format($w_rate_float,2)." ".$w_curr." - 1 BTC)";

	    $response = $telegram->sendMessage([
	        'chat_id' => $chatid, 
	        'text' => $message
	    ]);
	}

	public function getUserID($telegram, $chatid, $info = null){
	    $message = '';
	    if($info !== null){
	        $message .= $info.chr(10);
	    }
	    $response = $telegram->sendMessage([
	        'chat_id' => $chatid, 
	        'text' => $message
	    ]);
	}
}
