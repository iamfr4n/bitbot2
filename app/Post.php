<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
     protected $table = 'bot_config';
     protected $fillable = ['user_id','bot_currency','telegram_username','contact_tel'];
     //protected $guarded = [];
}
