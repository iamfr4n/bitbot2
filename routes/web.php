<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/bot-config', 'BotConfigController@show');
//Route::get('/bot-config/create', 'PostController@create');
Route::post('/bot-config', 'BotConfigController@store');

Route::get('me', 'ApiController@me');
Route::get('updates', 'ApiController@updates');
Route::get('respond', 'ApiController@respond');

Route::get('setWebHook', 'ApiController@setWebHook');
Route::post('331819316:AAHu685JGc6zRiLwHIHkzVVyKdgjUWBUarU/webhook', 'ApiController@webhook');
