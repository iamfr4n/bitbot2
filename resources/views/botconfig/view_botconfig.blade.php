@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form method="POST" action="/bot-config">
                <div class="panel panel-default">
                    <div class="panel-heading">Bot Config</div>
                    <div class="panel-body">
                            {{ csrf_field() }}
                            <div class="form-group col-md-4">
                                <label for="w_bot_currency">Default Currency</label>
                                <select class="form-control" id="w_bot_currency" name="w_bot_currency">
                                    <option value="">Select a currency</option>
                                    @if (isset($currencies))
                                    @foreach($currencies as $key => $value)
                                        <option value="{{$key}}" {{ ($data->bot_currency == $key) ? ' selected="selected"' : '' }}>{{$value}}</option>

                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6">
                                <label for="w_telegram_username">Telegram Username</label>
                                <input type="text" class="form-control" id="w_telegram_username" name="w_telegram_username" value="{{ $data->telegram_username }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="w_user_contact_tel">Contact Nr</label>
                                <input type="text" class="form-control" id="w_user_contact_tel" name="w_user_contact_tel" value="{{ $data->contact_tel }}">
                            </div>
                    </div>
                    <div class="panel-footer text-right">
                        <button type="reset" class="btn btn-sm btn-default">Clear</button>
                        <button type="submit" class="btn btn-sm btn-success">Save Changes</button>
                    </div>
                </div>
            </form>            
        </div>
    </div>
</div>
@endsection