@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">Welcome</div>
                <div class="panel-body">
                    <div class="media">
                        <div class="media-left">
                            <img class="media-object img-thumbnail" src="{{ url('images/bender.png') }}">
                        </div> 
                        <div class="media-body">
                            <h4 class="media-heading">Welcome to BitBot II<br>
                            <small>BitBot is a <a href="https://telegram.org/" title="Telegram Messenger">Telegram Messenger</a>bot which returns BitCoin Rates and other information.</small>
                            </h4>
                            <p>You can register on this website and link your <a href="https://telegram.org/" title="Telegram Messenger">Telegram Messenger</a> username to your <a href="https://payb.ee/" title="PayBee">PayBee</a> account to set up default configurations for BitBot.</p>
                            <p>To download BitBot for Telegram please visit : <a href="https://telegram.me/iamfr4n_bot" target="_new">@iamfr4n_bot</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection